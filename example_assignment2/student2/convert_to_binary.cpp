// student 2
#include "convert_to_binary.h"

std::string convert_to_binary(int num2convert) 
{
  if (num2convert == 0) {
    return "0";
  }
  std::string result = "";

  while (num2convert > 0) {
    if (num2convert % 2 == 0) {
      result = "0" + result;
    } else {
      result = "1" + result;
    }
    num2convert /= 2;
  }
// comments help cheaters!
  return result;
}

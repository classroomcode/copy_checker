// student 2
#include <iostream>

#include "convert_to_binary.h"

int main() {
  int num_to_convert;
  std::cin >> num_to_convert;
  std::cout << convert_to_binary(num_to_convert) << std::endl;
}

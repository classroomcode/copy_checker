// student 3
#include "convert_to_binary.h"

std::string convert_to_binary(int num_to_convert) {
 if (num_to_convert == 0) {
 return "0";
 }
 std::string result = "";

 while (num_to_convert > 0) {
 if (num_to_convert % 2 == 0) {
      result = "0" + result;
 } else {
      result = "1" + result;
    }
// what terrible spacing
    num_to_convert /= 2;
  }
  return result;
}

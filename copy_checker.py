#!/usr/bin/python3
# -*- coding: utf-8 -*-

# TODO include an edit distance option (from gutenberg-sec autograder / fuzzydiffer)?

# TODO parameter for selecting distance metric?

# TODO Make auto-format command more modular using an arg of dict of ext-command.

# TODO Do one report with, and one without auto-reformat, rather than overwrite?

# TODO Do a report/scan with and without comments?
# Sometimes they"re incriminating, sometimes they"re evasive.

# TODO Rust support
"""
Copyright behindthebrain@zoho.eu 2016-2020

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see: https://www.gnu.org/licenses/gpl-3.0.html
"""

from os import getcwd, mkdir
from subprocess import run
from glob import glob
from datetime import datetime
from typing import Dict, List
import filecmp

# Levenshtein edit distance
# pip3 install python-Levenshtein
# dnf/zypper install python3-Levenshtein
# apt install python3-levenshtein
import Levenshtein  # type: ignore

# Not in use currently, since Levenshtein may be more robust
# from difflib import SequenceMatcher


def potential_cheaters(
    rel_path: str = "example_assignment1/",
    file_include: List[str] = [
        ".cpp",
        ".hpp",
        ".h",
        ".py",
        ".sh",
        "specific_file.py",
        "specific_file_all_extensions",
    ],
    file_exclude: List[str] = ["maze.h", "unit_test_utilities.h", ".html", ".bak"],
    original_dist_files: List[str] = [
        "../full/path/to/template_file_yougave1.py",
        "../path2/file2.py",
    ],
    SENSITIVITY_LOW: float = 0.75,
    SENSITIVITY_HIGH: float = 1.01,
    reformat_code: bool = True,
    format_dict: Dict[str, str] = {
        "py": "black",
        "sh": "shfmt -i 4 -w",
        "c": "clang-format -i --Werror",
        "h": "clang-format -i --Werror",
        "cpp": "clang-format -i --Werror",
        "hpp": "clang-format -i --Werror",
    },
) -> None:
    """
    This little program checks similarity between all files of a specified type(s),
    in all sub-directories of the directory it is run in.
    It is slow by design, and it is in python...
        Where:
            n = number of glob hits for files to check
            a = numer of assignments, or calls to potential_cheaters
        The runtime is:
            a * ((n**2) / 2) - n)

    It can be run on its own by executing this file itself:
        $ python3 copy_checker.py

    or imported in python:
        See example wrapper.py for a multiprocessed version to speed up grading.
        >>> from copy_checker import potential_cheaters
        >>> potential_cheaters()

    Inputs:
        SENSITIVITY_LOW     -- Is a numeric proportion, ranging from 0-1
                               Change the SENSITIVITY value to adjust sensitivity threshold.
                               Lower includes more suspected hits.

        SENSITIVITY_HIGH    -- Is a numeric proportion, defaults to 1.01
                               This 1.01 is default, because, if you have students submit empty files
                               then putting it at 0.999 can ignore those, but normally it includes 1.
                               However, use 0.99 with caution, since students who submit exact
                               copies will be missed (it's more common than you think).
                               It's better to use the original_dist_files arg below.

        file_include        -- An array of the extension or search-term, as follows:
                               [".cpp", ".h", ".hpp", ".txt", ".py", ".py3", ".c"]
                               Can be a specific file as well (maybe easier than negative list)
                               Can also be a specific file name without the extension, to compare all
                               file.cpp and file.h in one pool.
                               Can just be one item list, for example: [".cpp"]

        file_exclude        -- Specific filenames to exclude, each as an element of an array
                               This is helpful for files we give out, but do not want to check.

        reformat_code       -- Boolean, whether or not to reformat the student code before checking,
                               to improve evasion detection. Python and C/C++ supported for now.

        format_dict         -- Maps the file extension to auto-formatter. You can add your own!

        rel_path            -- Relative path to the script's parent directory, that you would like
                                to execute globbing for files in.

        original_dist_files -- The files you gave out as templates that they should edit.
                               Setting this eliminates false alarms between students who didn't even touch the assignment.

    Outputs
        A folder dated with the run-time, with the following in it:
            * A summary file detailing the parameters of the run, and the potential hits:
              all_candidate_cheaters_summary.txt
            * For any candidate cheater pairs, a diff file,
              named by their joined usernames/foldernames:
              uname1_uname1_diff.whateverextensionyoupicked
            * For any candidate cheater pairs, so you can send a cheating report,
              a pretty html vimdiff file, named by their joined usernames:
              uname1_uname1_vimdiff.html
              This relies on reasonable coloring in vimdiff;
              see the .vimrc (name dotvimrc) in the repo.
              Execute this to use it: $ cp dotvimrc ~/.vimrc
    """

    disclaimer = """DISCLAIMER:
    * This report compares text files for similarity,
      and thus suggests candidates to check for potential cheating.
    * Naturally, there will be hits, false alarms, correct rejections, and misses.
    * The point of this program is to narrow the pool of potential cheaters for you to check by hand.
    * You MUST check the below potential hits by hand to decide whether you think they cheated!

    * I suggest using any of the following to compare cheaters:
        * The diff files this program outputs, which use $diff (no coloring of diffs)
        * http://meldmerge.org/ (the most flexible, nicest): meld --diff file1 file2
        * http://kdiff3.sourceforge.net/ (similar to meld)
        * vim -d file1.cpp file2.cpp (if you already know what you are doing)

    * For any questions, email: behindthebrain@zoho.eu

    * The similarity ratios generated map to certainty, in my observation, about like this:
        0.9 or greater is basically certain to be copy-pasted
        0.8 or greater is very likely
        0.7 quite likely
        0.6 likely
        0.5 borderline, usually copy-paste, but with edits or intent to conceal

    * Common sources of false alarms include:
        -- A student who submitted their assignment to multiple sections on accident
        -- Files we give out (which you can exclude in the source code).
        -- Groupwork, where it helps to have only one of the group or pair submit instead.
        -- Students who turn in empty file or empty templates you distributed.
        -- Code written for an assignment which dictates a small or rigid solution.

    * If you see Python exceptions below, chances are the student used a bad file encoding
      (i.e., saved their text file in Windows rather than *nix eol, UTF-8).
      If you want, open and re-save their file UTF-8/Unix delimited and try again.
    """

    # added last superfolder as extension at end,
    # to avoid non-unique colliding foldernames for Pool() wrapped mp
    report_time_stamp_name = (
        "run_"
        + str(datetime.today()).replace(" ", "_")[0:19]
        + "_potential_cheaters_"
        + rel_path.split("/")[-2]
    )
    mkdir(report_time_stamp_name)

    with open(
        report_time_stamp_name + "/all_candidate_cheaters_summary.txt", "w"
    ) as out_file:
        out_file.write(
            "Report of potential cheaters generated:\n\t "
            + str(datetime.today())[0:19]
            + "\n\nSensitivity threshold set at:\n\t "
            + str(SENSITIVITY_LOW)
            + " to "
            + str(SENSITIVITY_HIGH)
            + "\n\nCurrent working directory was:\n\t "
            + getcwd()
            + "\n\nFile types / file list was:\n\t "
            + str(file_include)
            + "\n\nFile exclude list was:\n\t "
            + str(file_exclude)
            + "\n\nRelative path to all that was:\n\t "
            + rel_path
            + "\n\n"
            + disclaimer
        )

        for file_search in file_include:
            out_file.write(
                "\n\n====================================== \t File type "
                + file_search
                + " \t======================================\n"
            )

            filenames = glob(rel_path + "**/*" + file_search + "*", recursive=True)
            filenames = [
                i for i in filenames if not any([ex for ex in file_exclude if ex in i])
            ]

            for fn1 in filenames:
                for fn2 in filenames[filenames.index(fn1) + 1 :]:

                    # TODO autoformat and order can mess this little section up...
                    studentdidnothing = False
                    for given_file in original_dist_files:
                        if filecmp.cmp(fn1, given_file) or filecmp.cmp(fn2, given_file):
                            studentdidnothing = True
                            break
                    if studentdidnothing:
                        continue

                    if reformat_code:
                        fn1_ext = fn1.split(".")[-1]
                        fn2_ext = fn2.split(".")[-1]
                        if fn1_ext in format_dict:
                            run(format_dict[fn1_ext].split() + [fn1])
                        if fn2_ext in format_dict:
                            run(format_dict[fn2_ext].split() + [fn2])

                    with open(fn1) as file_1, open(fn2) as file_2:
                        try:
                            file1_data = file_1.read().upper()
                            file2_data = file_2.read().upper()
                        except Exception as file_open_exception:
                            out_file.write(
                                "\n\nFiles:\n\t"
                                + fn1
                                + "\n\t"
                                + fn2
                                + "\nhad exception:\n\t"
                                + str(file_open_exception)
                            )

                        similarity_ratio = Levenshtein.ratio(file1_data, file2_data)
                        # similarity_ratio = SequenceMatcher(None, file1_data, file2_data).ratio()

                        if SENSITIVITY_LOW < similarity_ratio < SENSITIVITY_HIGH:
                            out_file.write(
                                "\n\nFiles:\n\t"
                                + fn1
                                + "\n\t"
                                + fn2
                                + "\nhave similarity:\n\t"
                                + str(round(similarity_ratio, 2))
                            )

                            max_line_len_array = file1_data.split(
                                "\n"
                            ) + file2_data.split("\n")
                            max_line_len = max(
                                [len(line) for line in max_line_len_array]
                            )
                            diff_basename = (
                                report_time_stamp_name
                                + "/"
                                + fn1.split("/")[-2]
                                + "_"
                                + fn1.split("/")[-1].replace(".", "-")
                                + "_"
                                + fn2.split("/")[-2]
                                + "_"
                                + fn2.split("/")[-1].replace(".", "-")
                            )

                            # TODO in creating the extension of the diff file,
                            # matched files could have different extensions... though it"s quite unlikely
                            # This would be visible in the diff_basename, by a human
                            diff_file_ext = fn1.split(".")[-1]
                            with open(
                                diff_basename + "_diff." + diff_file_ext, "wb", 0
                            ) as output_file:
                                run(
                                    [
                                        "diff",
                                        "-y",
                                        "-W",
                                        str(max_line_len * 2 + 50),
                                        fn1,
                                        fn2,
                                    ],
                                    stdout=output_file,
                                )

                            run(
                                [
                                    "vimdiff",
                                    fn1,
                                    fn2,  # Good colors here via -c 'etc.,'
                                    "-c",
                                    "TOhtml",
                                    "-c",
                                    "w! " + diff_basename + "_vimdiff.html",
                                    "-c",
                                    "qa!",
                                ]
                            )


if __name__ == "__main__":
    potential_cheaters()

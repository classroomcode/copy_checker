#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
This is a working space to run each instance for a class,
so you don"t have to edit copy_checker.py defaults every time.

Just run all check checking instances in this directory, changing the rel_path,
to keep copies of the code to a minimum.
The .gitignore will ignore any run-data you end up with.

Copyright behindthebrain@zoho.eu 2016-2020

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see: https://www.gnu.org/licenses/gpl-3.0.html
"""

from copy_checker import potential_cheaters
from multiprocessing import Pool


# Defaults
# potential_cheaters()

potential_cheaters(
    rel_path="../../../gitlab-classes-mst/2021-SP-CS1500/student_submissions/pa03-word-up/",
    file_include=["word_up.py"],
    original_dist_files=[
        "../../../gitlab-classes-mst/2021-SP-CS1500/pa03-word-up/word_up_old.py"
    ],
    SENSITIVITY_LOW=0.8,
)

# potential_cheaters(
#     rel_path="../../../gitlab-classes-mst/2021-SP-CS1500/student_submissions/pa02-race-condition/",
#     file_include=["race_condition.py"],
#     original_dist_files=[
#         "../../../gitlab-classes-mst/2021-SP-CS1500/pa02-race-condition/race_condition.py"
#     ],
#     SENSITIVITY_LOW=0.7,
# )

# potential_cheaters(
#     rel_path="../../../gitlab-classes-mst/2021-SP-CS4610/student_submissions/pa01-bash-caesar/",
#     file_include=["bash-caesar.sh"],
#     original_dist_files=[
#         "../../../gitlab-classes-mst/2021-SP-CS4610/pa01-bash-caesar/bash-caesar.sh"
#     ],
#     SENSITIVITY_LOW=0.60,
# )

# comp pa02
# potential_cheaters(
#     rel_path="../../../gitlab-classes-mst/2020-FS-CS1500/student_submissions/pa02-tunnel-vision/",
#     file_include=["racer.py"],
#     original_dist_files=[
#         "../../../gitlab-classes-mst/2020-FS-CS1500/pa02-tunnel-vision/racer.py"
#     ],
#     SENSITIVITY_LOW=0.80,
# )

# sec-pa01
# potential_cheaters(
#     rel_path="../../2020-FS-CS4610/student_submissions/pa01-bash-caesar_/",
#     file_include=["bash-caesar.sh", "bash-caesar-2.sh"],
#     file_exclude=[],
#     SENSITIVITY_LOW=0.55,
# )

# Security single-core sequential run example
# potential_cheaters(SENSITIVITY_LOW=0.7,
#                   file_include=["betterSubCrack.py"],
#                   rel_path="../2019-SP-CS3600-A/student_submissions/pa01-frequency/")
#
# potential_cheaters(SENSITIVITY_LOW=0.7,
#                   file_include=["bin_otp_key_gen.py", "bin_otp.py"],
#                   rel_path="../2019-SP-CS3600-A/student_submissions/pa02-otp/")
#
# potential_cheaters(SENSITIVITY_LOW=0.7,
#                   file_include=[".py", ".sh"],
#                   rel_path="../2019-SP-CS3600-A/student_submissions/pa05/")


# Data Structures single-core sequential run example
# potential_cheaters(SENSITIVITY_LOW=0.7,
#                   SENSITIVITY_HIGH=1,
#                   file_include=["matrix_search.cpp"],
#                   rel_path="../2019-SP-CS1575-A/student_submissions/pa01/")
#
# potential_cheaters(SENSITIVITY_LOW=0.70,
#                    SENSITIVITY_HIGH=1,
#                    file_include=["word_up.py"],
#                    rel_path="../2020-SP-CS1500/student_submissions/pa03-word-up/")


# If you are running multiple assignments at once,
# then you can put one call to potential_cheaters (typically one assignment) on each core, like below.
# Edit Pool(n) such that n is the numbef or cores on your machine, e.g., Pool(8)
# Note: you can't make a call for potential_cheaters for each file in one assignment, and mp across files,
# because it's all writing to the same file.

# Data Structures mp
# TODO changed order, so these need updating
# args_array = [(0.7, 1, ["matrix_search.cpp"], [], "../2019-SP-CS1575-A/student_submissions/pa01/"),
#              (0.7, 1, ["maze.cpp"], [], "../2019-SP-CS1575-A/student_submissions/pa03/"),
#              (0.7, 1, ["MyVector.hpp"], [], "../2019-SP-CS1575-A/student_submissions/pa04/"),
#              (0.7, 1, ["MyList.hpp"], [], "../2019-SP-CS1575-A/student_submissions/pa05/"),
#              (0.7, 1, ["MyMap.hpp"], [], "../2019-SP-CS1575-A/student_submissions/pa06/"),
#              (0.7, 1, ["MyHeap"], [], "../2019-SP-CS1575-A/student_submissions/pa07/"),
#              (0.7, 1, ["MyUnorderedMap.hpp"], [], "../2019-SP-CS1575-A/student_submissions/pa08/"),
#              (0.7, 1, ["MyGraph.h", "import_graph.cpp", "dijkstra.cpp",], [], "../2019-SP-CS1575-A/student_submissions/pa09/")]

# args_array = [("example_assignment1/", [".py"]), ("example_assignment2/", [".py"])]

# Yes, my grading VM only has two cores...
# with Pool(2) as p:
#     p.starmap(potential_cheaters, args_array)

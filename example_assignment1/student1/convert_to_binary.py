# student 1
def convert_to_binary(num: int) -> str:
    if num == 0:
        return "0"
    result = ""
    while num > 0:
        if num % 2 == 0:
            result += "0"
        else:
            result += "1"
        num //= 2
    return result[::-1]


if __name__ == "__main__":
    num_to_convert = int(input())
    print(convert_to_binary(num_to_convert))

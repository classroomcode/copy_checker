#!/bin/bash

# student 1

dec2bin() {
    # https://stackoverflow.com/questions/10278513/bash-shell-decimal-to-binary-base-2-conversion
    # https://stackoverflow.com/questions/44738494/understanding-code-0-10-10-10-10-10-10-10-1
    D2B=({0..1}{0..1}{0..1}{0..1}{0..1}{0..1}{0..1}{0..1})

    # Strips leading 0's
    echo $((10#${D2B[$1]}))
}

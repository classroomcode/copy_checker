# Copy Checker
A simple crude plagiarism detection program for source code.

![](cheating.gif)

# Documentation
See the function docstring and disclaimer text generated in the dated output report for documentation.

# How to run this code?
Just clone this folder, and edit the wrapper, and run from within the repo:
```sh
python3 wrapper.py
```

# Dependencies and assumptions
Assumes:
* Running on Linux, with bash, Vim, Meld, python3-pudb, python3-black, clang-format, shfmt, and more (see the script)!
* You have reasonable vimdiff colors (see mine if you want in the repo).
* A tree folder structure, where one super-folder has within it, one folder for each student; see `example_assignment/`

# Contributing
With any fixes or improvements, please feel free to fork, edit, and pull request, or simply create an issue on the issues page.

Auto-format the python code before committing:
```python
black *.py
```

Use type hints, either manually, or automatically using monkeytype, and check using mypy:
```sh
monkeytype run wrapper.py
monkeytype apply copy_checker
mypy --strict --disallow-any-explicit *.py
```

# License
Copyright behindthebrain@zoho.eu 2016-2020

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see: https://www.gnu.org/licenses/gpl-3.0.html

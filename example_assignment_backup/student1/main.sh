#!/bin/bash

source convert_to_binary.sh

# https://www.reddit.com/r/bash/comments/gom3hp/while_read_options_r_no_r_etc/
read -r user_in
dec2bin "$user_in"
